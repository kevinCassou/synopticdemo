#!/usr/bin/env python


from setuptools import setup


setup(
    name="taurusgui-synopticdemo",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    description="Synoptic demo application.",
    author="MAX IV",
    author_email="kits-sw@maxiv.lu.se",
    license="GPLv3",
    url="https://gitlab.com/MaxIV/synopticdemo",
    packages=["synopticdemo"],
    package_data={
        "synopticdemo": [
            "resources/index.html",
            "resources/images/*.svg",
            "resources/images/*.png",
        ]
    },
    entry_points={
        "console_scripts": [
            "ctsynopticdemo=synopticdemo.__main__:main",
        ]
    },
    python_requires=">=3.6",
    install_requires=["pytango", "taurus", "svgsynoptic2"],
    extras_require={
        "tests": [
            "pytest",
        ],
    },
)
